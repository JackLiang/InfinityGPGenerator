本软件是无垠式java通用代码生成器0.8（Code Name:Trinity 崔妮蒂）的全部源码。
本软件是Java Web通用代码生成器平台，是通用的加快开发速度的工具。支持s2sh,s2shc和simplejee三个技术栈。
开发环境是 Java 7开发工具 Eclipse JEE版全部源码在GPL v2版条款下开源，GPL v2的文本记录在gpl2.txt中本软件包含三个技术栈：Clocksimplejee(jsp,simplejee)栈，s2sh栈和s2shc栈。
希望大家群策群力，把动词算子式通用代码生成器的使用在更广阔的场景中。
可以扩展新的技术栈：现有的除了上面三个，还有和平之翼代码生成器的s2sm,sm和shc栈，和正在研发中的sj(SpringJPA)栈。
可以支持更多的数据库，无垠式代码生成器现在只支持mysql和hibernate。现在，仅有和平之翼代码生成器2.0,3.0版支持Oracle。
可有配上更好的前端界面，我现在正视图将前端改成JQuery Easy UI使本软件支持父子表，自动外键支持，一对一，一对多，多对多。
本软件系列中的和平之翼代码生成器SMEU 3.0版（乌篷船）已支持上述先进特性。同时，乌篷船支持Excel模板代码生成。
软件界面截图

![输入图片说明](https://gitee.com/uploads/images/2018/0209/094851_8819da42_1203742.jpeg "infinityGPGenerator.jpg")


崔妮蒂

![输入图片说明](https://gitee.com/uploads/images/2018/0206/142003_9fd7a7af_1203742.jpeg "Trinity.jpg")